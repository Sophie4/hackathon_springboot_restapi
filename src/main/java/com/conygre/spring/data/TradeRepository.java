package com.conygre.spring.data;

import com.conygre.spring.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

 public interface TradeRepository extends MongoRepository<Trade, ObjectId> {

// 	public List<Trade> findByDateCreated(String dateCreatedString);

// 	@Query("{'artist': ?0}")
// 	public List<Trade> customFindBydateCreated(String s);

}