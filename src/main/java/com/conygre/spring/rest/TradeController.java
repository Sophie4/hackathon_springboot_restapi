package com.conygre.spring.rest;

import com.conygre.spring.service.TradeService;

import java.util.Optional;

import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/trades")
@CrossOrigin // allows requests from all domains
public class TradeController {

	// private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private TradeService service;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Trade> findAll() {
		// logger.info("managed to call a Get request for findAll");
		return service.getCatalog();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Optional<Trade> getTradeById(@PathVariable("id") String id) {
		return service.getTradeById(new ObjectId(id));
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteTrade(@PathVariable("id") String id) {
		service.deleteTrade(new ObjectId(""+id));
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteTrade(@RequestBody Trade trade) {
		service.deleteTrade(trade);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void addTrade(@RequestBody Trade trade) {
		service.addToCatalog(trade);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public void updateTrade(@RequestBody Trade trade) {
		service.updateTrade(trade);
	}

}