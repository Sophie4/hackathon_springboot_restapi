package com.conygre.spring.service;

import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.Optional;

@Service
public class TradeService {

    @Autowired
    private TradeRepository trade_repo;


    public void addToCatalog(Trade trade) {
        trade_repo.insert(trade);
    }

    public Collection<Trade> getCatalog() {
        return trade_repo.findAll();
    }

    public Optional<Trade> getTradeById(ObjectId id) {
        return trade_repo.findById(id);
    }

    public void deleteTrade(ObjectId id) {
        trade_repo.deleteById(id);
    }

    public void deleteTrade(Trade trade) {
        trade_repo.delete(trade);
    }

    public void updateTrade(Trade trade) {
        System.out.println(trade.getStockQuantity());
        trade_repo.save(trade);
    }


}