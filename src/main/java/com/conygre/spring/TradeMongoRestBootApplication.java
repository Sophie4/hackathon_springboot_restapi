package com.conygre.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeMongoRestBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeMongoRestBootApplication.class, args);
	}

}
