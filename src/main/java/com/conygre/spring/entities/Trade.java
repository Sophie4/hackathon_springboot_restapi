package com.conygre.spring.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Document
public class Trade {

  @Id 
  private String id;
  private String dateCreated;
  private String timeCreated;
  private String stockTicker;
  private double stockQuantity;
  private double requestedPrice;
  private String tradeStatus;
  private String tradeType;
  private boolean historic;

  public Trade(String id, String dateCreated, String timeCreated, String stockTicker, double stockQuantity, double requestedPrice,
      String tradeStatus, String tradeType, boolean historic) {
    this.id = id;
    this.dateCreated = dateCreated;
    this.timeCreated = timeCreated;
    this.stockTicker = stockTicker;
    this.stockQuantity = stockQuantity;
    this.requestedPrice = requestedPrice;
    this.tradeStatus = tradeStatus;
    this.tradeType = tradeType;
    this.historic = historic;
  }

  public Trade() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(String dateCreated) {
    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
    this.dateCreated = dateFormatter.format(LocalDate.now());
  }

  public String getTimeCreated() {
    return timeCreated;
  }

  public void setTimeCreated(String timeCreated) {
   DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss"); 
   this.timeCreated = timeFormatter.format(LocalTime.now());
  }

  public String getStockTicker() {
    return stockTicker;
  }

  public void setStockTicker(String stockTicker) {
    this.stockTicker = stockTicker;
  }

  public double getStockQuantity() {
    return stockQuantity;
  }

  public void setStockQuantity(double stockQuantity) {
    this.stockQuantity = stockQuantity;
  }

  public double getRequestedPrice() {
    return requestedPrice;
  }

  public void setRequestedPrice(double requestedPrice) {
    this.requestedPrice = requestedPrice;
  }

  public String getTradeStatus() {
    return tradeStatus;
  }

  public void setTradeStatus(String tradeStatus) {
    this.tradeStatus = tradeStatus;
  }

  public String getTradeType() {
    return tradeType;
  }

  public void setTradeType(String tradeType) {
    this.tradeType = tradeType;
  }

  public boolean getHistoric() {
    return historic;
  }

  public void setHistoric(boolean historic) {
    this.historic = historic;
  }

}
